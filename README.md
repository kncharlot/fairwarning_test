# README #
This demo application is a spring boot demo using H2 in memory database to monitor visitor activity.
The front end is a simple thymeleaf template set of pages. 
There is a swagger ui to see the available endpoints, requests, responses and models for the API.


## To Build the App ##
Pull this repository into a local directory. 
From a terminal run mvn clean install. This will build the project and copy the new runnable jar in the jar-to-run directory.
Run this command to run the jar with the desired port.
java -Dserver.port=desired_port_number -jar executable.jar

## Otherwise ##
Just download the jar if your organisation allows it and run the same command up above from a terminal inside the directory where you downloaded the jar.

## Running the App ##
Once the app is running,
Go to http://localhost:8080/swagger-ui.html#/ to see the swagger UI for the API.
Go to http://localhost:8080/ to start interfacing with the app.
You can also curl but, honestly, who wants to do that? :)

You will need an admin credentials to enable some functionalities.
Try to guess what the "admin" firstname and "admin" lastname are.
Good luck and have fun.
Thanks


### Who do I talk to? ###
Charles Kossivi kncharlot@gmail.com
