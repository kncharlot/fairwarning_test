package com.fair.warning.demo.visitorappdemo.service.impl;

import com.fair.warning.demo.visitorappdemo.model.PingInfo;
import com.fair.warning.demo.visitorappdemo.service.PingService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class PingServiceImpl implements PingService {
    private String status="UP";

    @Value("${info.build.version}")
    private  String version;

    @Value("${info.build.artifact}")
    private  String artifact;

    @Value("${info.build.name}")
    private  String name;

    @Value("${info.build.description}")
    private  String description;

    public PingInfo ping() {
        return new PingInfo(status, version, artifact, name, description);
    }
}
