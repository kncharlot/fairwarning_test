package com.fair.warning.demo.visitorappdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VisitorAppDemoApplication {
    public static void main(String[] args) {
        SpringApplication.run(VisitorAppDemoApplication.class, args);
    }
}
