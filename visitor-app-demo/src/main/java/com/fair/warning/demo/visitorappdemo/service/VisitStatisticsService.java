package com.fair.warning.demo.visitorappdemo.service;

import com.fair.warning.demo.visitorappdemo.model.VisitStatistics;

public interface VisitStatisticsService {
    VisitStatistics buildVisitStatistics();

}
