package com.fair.warning.demo.visitorappdemo.repository;


import com.fair.warning.demo.visitorappdemo.model.ErrorDetails;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ErrorDetailsRepository extends CrudRepository<ErrorDetails, Long> {
    ErrorDetails findErrorDetailsById(long id);
}
