package com.fair.warning.demo.visitorappdemo.service;

import com.fair.warning.demo.visitorappdemo.model.PingInfo;

public interface PingService {
    PingInfo ping();
}
