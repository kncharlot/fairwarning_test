package com.fair.warning.demo.visitorappdemo.controller;

import com.fair.warning.demo.visitorappdemo.exception.ApiException;
import com.fair.warning.demo.visitorappdemo.model.ErrorDetails;
import com.fair.warning.demo.visitorappdemo.model.Visitor;
import com.fair.warning.demo.visitorappdemo.service.ErrorDetailsService;
import com.fair.warning.demo.visitorappdemo.service.VisitStatisticsService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.util.Date;

@ControllerAdvice
public class ApiExceptionController {
    @Autowired
    private ErrorDetailsService errorDetailsService;
    @Autowired
    private VisitStatisticsService visitStatisticsService;
    private static final Logger LOG = LoggerFactory.getLogger(ApiExceptionController.class);

    @ExceptionHandler({Exception.class, ApiException.class})
    public String handleException(ApiException ex, WebRequest request, Model model) {
        LOG.info("handleException()");
        String errorPage = "error";
        ErrorDetails errorDetails = errorDetailsService.saveError(ErrorDetails.builder()
                .timestamp(new Date())
                .message(ex.getMessage())
                .details(request.getDescription(false))
                .build());

        if (StringUtils.containsIgnoreCase(ex.getMessage(), "cache")) {
            LOG.info("Cache building problems. Trying again.");
            try {
                visitStatisticsService.buildVisitStatistics();
                errorPage = "redirect:/home";
            } catch (Exception e) {
                LOG.error("Cache build failed again {}", e.getMessage(), e);
                errorDetails = errorDetailsService.saveError(ErrorDetails.builder()
                        .timestamp(new Date())
                        .message(ex.getMessage())
                        .details(request.getDescription(false))
                        .build());
            }
        }
        model.addAttribute("processId", errorDetails.getId());
        model.addAttribute("visitor", new Visitor());
        return errorPage;
    }
}