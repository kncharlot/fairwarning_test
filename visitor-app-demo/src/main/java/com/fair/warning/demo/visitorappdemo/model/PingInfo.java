package com.fair.warning.demo.visitorappdemo.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PingInfo extends BaseModel {
    @ApiModelProperty(notes = "The status of the App.")
    private String status;
    @ApiModelProperty(notes = "The version of build of the App.")
    private String version;
    @ApiModelProperty(notes = "The artifact id of the App.")
    private String artifact;
    @ApiModelProperty(notes = "The name of the App.")
    private String name;
    @ApiModelProperty(notes = "The description of the App.")
    private String description;

}
