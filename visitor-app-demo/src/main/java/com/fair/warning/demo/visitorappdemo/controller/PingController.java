package com.fair.warning.demo.visitorappdemo.controller;

import com.fair.warning.demo.visitorappdemo.model.PingInfo;
import com.fair.warning.demo.visitorappdemo.service.PingService;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;

@RestController
public class PingController {
    private static final Logger LOG = LoggerFactory.getLogger(PingController.class);

    @Autowired
    PingService pingService;

    @PostConstruct
    public void initialize() {
        LOG.info("{} REST service has been initialized", this.getClass().getName());
    }

    @ApiOperation(value = "Use to check status of mapping service")
    @GetMapping(value = "/ping")
    public ResponseEntity<PingInfo> ping() {
        LOG.info("Received ping");
        PingInfo pingInfo = pingService.ping();
        return new ResponseEntity(pingInfo, HttpStatus.OK);
    }
}
