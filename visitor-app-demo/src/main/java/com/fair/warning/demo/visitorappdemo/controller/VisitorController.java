package com.fair.warning.demo.visitorappdemo.controller;

import com.fair.warning.demo.visitorappdemo.Util.StatUtil;
import com.fair.warning.demo.visitorappdemo.Util.Util;
import com.fair.warning.demo.visitorappdemo.model.Visitor;
import com.fair.warning.demo.visitorappdemo.model.VisitorDto;
import com.fair.warning.demo.visitorappdemo.service.VisitorService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class VisitorController {

    private static final Logger LOG = LoggerFactory.getLogger(VisitorController.class);
    private static final String welcomeMessage = "Welcome";
    @Autowired
    private VisitorService visitorService;

    @GetMapping({"/", "/home"})
    public String welcome(Model model) {
        VisitorDto visitor = new VisitorDto();
        model.addAttribute("welcomeMessage", welcomeMessage);
        model.addAttribute("alert", "Welcome friend.");
        model.addAttribute("visitor", visitor);
        model.addAttribute("noName", true);
        return "index";
    }

    @GetMapping("/greeting")
    public String greeting() {
        return "redirect:/home";
    }

    @PostMapping("/greeting")
    public String greeting(@Valid @ModelAttribute VisitorDto visitor, Model model) {
        LOG.info("greeting()");
        StringBuilder welcome = new StringBuilder("Welcome ");
        if (null == visitor || StringUtils.isAllEmpty(visitor.getUserFirst(), visitor.getUserLast())) {
            return "redirect:/home";
        } else {
            //Check cached list first.
            model.addAttribute("noName", false);
            try {
                Visitor returningVisitor = StatUtil.INSTANCE.getCachedVisitorsList().get(visitor.getUserFirst() + visitor.getUserLast());
                if (null != returningVisitor) {
                    LOG.info("Found it in cache.");
                    returningVisitor.setVisitCount(returningVisitor.getVisitCount() + 1);
                    returningVisitor = visitorService.saveOrUpdate(returningVisitor);
                } else {
                    visitor.setVisitCount(1);
                    returningVisitor = visitorService.saveOrUpdate((Visitor) Util.buildObjectFromOriginal(visitor, Visitor.class));
                }

                int sameLastNames = 0;
                int sameFirstNames = 0;
                try {
                    sameLastNames = visitorService.findVisitorsByLastName(visitor.getUserLast()).size();
                    sameFirstNames = visitorService.findVisitorsByFirstName(visitor.getUserFirst()).size();
                } catch (Exception e) {
                    LOG.error("Error {}", e.getMessage(), e);
                }

                //So we see the latest updated visitor every time;
                StatUtil.INSTANCE.updateCache(returningVisitor);
                visitor.setVisitCount(returningVisitor.getVisitCount());
                model.addAttribute("visitor", visitor);
                if (returningVisitor.getVisitCount() > 1)
                    welcome.append("back ");

                welcome.append(StringUtils.capitalize(returningVisitor.getUserFirst()))
                        .append(" ")
                        .append(StringUtils.capitalize(returningVisitor.getUserLast()));
                model.addAttribute("alert", welcome.toString());
                model.addAttribute("sameFirstNames", (sameFirstNames > 0) ? sameFirstNames - 1 : sameFirstNames);
                model.addAttribute("sameLastNames", (sameLastNames > 0) ? sameLastNames - 1 : sameLastNames);
            } catch (Exception e) {
                LOG.error("Error {}", e.getMessage(), e);
            }
            return "index";
        }
    }

    @GetMapping("/reset")
    public String reset() {
        return "redirect:/home";
    }
}
