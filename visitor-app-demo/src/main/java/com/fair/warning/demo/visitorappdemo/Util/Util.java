package com.fair.warning.demo.visitorappdemo.Util;

import com.fair.warning.demo.visitorappdemo.exception.ApiException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.util.List;

public class Util {
    private static final Logger LOG = LoggerFactory.getLogger(Util.class);

    /**
     * Print class into json-ish string
     *
     * @param object
     * @return
     */
    public static String classToString(Object object) throws ApiException {
        try {
            Class clazz = object.getClass();
            Field[] fields = clazz.getDeclaredFields();
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer
                    .append("{\n");
            for (Field field : fields) {
                field.setAccessible(true);
                String fieldName = field.getName().toLowerCase();
                if (!StringUtils.equalsIgnoreCase(fieldName, "id"))
                    try {
                        Object value = field.get(object);
                        stringBuffer.append("\"" + fieldName + "\":");
                        if (null != value) {
                            String valueStr = value.toString();
                            if (isValidString(valueStr))
                                if (NumberUtils.isCreatable(valueStr))
                                    stringBuffer.append(value);
                                else
                                    stringBuffer.append("\"" + value + "\"");
                            else
                                stringBuffer.append("\"\"");

                            stringBuffer.append(",").append("\n");
                        }
                    } catch (Exception ex) {
                        LOG.error("Failed to extract value for field name {}. Skipping.", fieldName, ex);
                    }
            }
            return StringUtils.removeEnd(stringBuffer.toString().trim(), ",") + "\n }";
        } catch (Exception e) {
            LOG.error("Failed to print class. {}", e.getMessage(), e);
        }
        return "";
    }

    public static <ORIGINAL> Object buildObjectFromOriginal(ORIGINAL dto, Class clazz) {
        Object instance = null;
        Class dtoCLass = dto.getClass();
        try {
            instance = clazz.newInstance();
            Field[] fields = clazz.getDeclaredFields();
            for (Field field :
                    fields) {
                field.setAccessible(true);
                if (!StringUtils.equalsIgnoreCase("id", field.getName()))
                    try {
                        Field dtoField = dtoCLass.getDeclaredField(field.getName());
                        dtoField.setAccessible(true);
                        Object value = dtoField.get(dto);
                        field.set(instance, value);
                    } catch (Exception e) {
                        LOG.error("Error {}", e.getMessage(), e);
                    }
            }
        } catch (Exception e) {
            LOG.error("Could not build instance because {}", e.getMessage(), e);
        }
        return instance;
    }

    public static boolean isValidString(String string) {
        return null != string && StringUtils.isNotEmpty(string) && !StringUtils.equalsIgnoreCase(string, "null");
    }

    public static boolean isValidList(List list) {
        return null != list && !list.isEmpty();
    }
}
