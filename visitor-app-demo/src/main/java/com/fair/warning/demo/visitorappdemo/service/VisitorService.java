package com.fair.warning.demo.visitorappdemo.service;

import com.fair.warning.demo.visitorappdemo.model.Visitor;

import java.util.List;

public interface VisitorService {
    Visitor findVisitorByFirstAndLastName(String first, String last);

    List<Visitor> findVisitorsByFirstName(String first);

    List<Visitor> findVisitorsByLastName(String last);

    List<Visitor> findVisitorsByVisitCount(int visitCount);

    List<Visitor> findVisitorsByVisitCountGreaterThan(int visitCount);

    List<Visitor> findVisitorsByVisitCountLessThan(int visitCount);

    List<Visitor> findAll();

    Visitor saveOrUpdate(Visitor visitor);
}
