package com.fair.warning.demo.visitorappdemo.service;

import com.fair.warning.demo.visitorappdemo.model.ErrorDetails;

import java.util.List;

public interface ErrorDetailsService {
    ErrorDetails findByProcessId(long processId);

    List<ErrorDetails> findAll();

    ErrorDetails saveError(ErrorDetails errorDetails);
}
