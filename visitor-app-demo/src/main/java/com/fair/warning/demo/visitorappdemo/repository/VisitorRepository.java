package com.fair.warning.demo.visitorappdemo.repository;

import com.fair.warning.demo.visitorappdemo.model.Visitor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VisitorRepository extends CrudRepository<Visitor, Long> {
    Visitor findDistinctByUserFirstAndAndUserLast(String userFirst, String userLast);

    List<Visitor> findDistinctByUserFirst(String userFirst);

    List<Visitor> findDistinctByUserLast(String userLast);

    List<Visitor> findDistinctByVisitCount(int visitCount);

    List<Visitor> findDistinctByVisitCountGreaterThan(int visitCount);

    List<Visitor> findDistinctByVisitCountLessThan(int visitCount);
}
