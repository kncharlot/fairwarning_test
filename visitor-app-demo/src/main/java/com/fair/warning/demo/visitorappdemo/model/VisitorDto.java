package com.fair.warning.demo.visitorappdemo.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class VisitorDto extends BaseModel implements Serializable {
    @ApiModelProperty(notes = "The number of visit.")
    private int visitCount;
    @ApiModelProperty(notes = "The visitor's first name.")
    private String userFirst;
    @ApiModelProperty(notes = "The visitor's last name.")
    private String userLast;
}
