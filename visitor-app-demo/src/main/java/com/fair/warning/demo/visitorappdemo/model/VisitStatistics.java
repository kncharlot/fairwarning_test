package com.fair.warning.demo.visitorappdemo.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class VisitStatistics extends BaseModel implements Serializable {
    @ApiModelProperty(notes = "The total number of visitors.")
    private int totalVisits;
    @ApiModelProperty(notes = "The visitors first name.")
    private List<String> visitorFirstNames;
    @ApiModelProperty(notes = "The visitors last name.")
    private List<String> visitorLastNames;
    @ApiModelProperty(notes = "The list of visitor.")
    private List<VisitorDto> visitors;
}
