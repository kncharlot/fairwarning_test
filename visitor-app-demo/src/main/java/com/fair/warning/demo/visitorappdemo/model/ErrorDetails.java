package com.fair.warning.demo.visitorappdemo.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "errorDetails")
public class ErrorDetails extends BaseModel {
    @ApiModelProperty(notes = "The generated id from the DB.")
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @ApiModelProperty(notes = "The time of the error.")
    private Date timestamp;
    @ApiModelProperty(notes = "The error message.")
    private String message;
    @ApiModelProperty(notes = "Additional details.")
    private String details;
}
