package com.fair.warning.demo.visitorappdemo.service.impl;

import com.fair.warning.demo.visitorappdemo.model.ErrorDetails;
import com.fair.warning.demo.visitorappdemo.repository.ErrorDetailsRepository;
import com.fair.warning.demo.visitorappdemo.service.ErrorDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ErrorDetailsServiceImpl implements ErrorDetailsService {
    @Autowired
    ErrorDetailsRepository errorDetailsRepository;

    @Override
    public ErrorDetails findByProcessId(long processId) {
        return errorDetailsRepository.findErrorDetailsById(processId);
    }

    @Override
    public List<ErrorDetails> findAll() {
        return ((List<ErrorDetails>) errorDetailsRepository.findAll());
    }

    @Override
    public ErrorDetails saveError(ErrorDetails errorDetails) {
        return errorDetailsRepository.save(errorDetails);
    }
}
