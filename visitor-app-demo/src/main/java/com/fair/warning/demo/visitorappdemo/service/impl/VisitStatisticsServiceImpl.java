package com.fair.warning.demo.visitorappdemo.service.impl;

import com.fair.warning.demo.visitorappdemo.Util.StatUtil;
import com.fair.warning.demo.visitorappdemo.model.VisitStatistics;
import com.fair.warning.demo.visitorappdemo.model.Visitor;
import com.fair.warning.demo.visitorappdemo.model.VisitorDto;
import com.fair.warning.demo.visitorappdemo.repository.VisitorRepository;
import com.fair.warning.demo.visitorappdemo.service.VisitStatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class VisitStatisticsServiceImpl implements VisitStatisticsService {
    @Autowired
    VisitorRepository visitorRepository;

    /**
     * Get visit stats and refresh cache
     *
     * @return
     */
    @Override
    public VisitStatistics buildVisitStatistics() {
        List<Visitor> visitors = (List<Visitor>) visitorRepository.findAll();
        List<VisitorDto> visitorDtos = new ArrayList<>();
        Map<String, Visitor> cachedVisitorsList = new HashMap<>();
        final int[] totalVisits = {0};
        List<String> visitorFirstNames = new ArrayList<>();
        List<String> visitorLastNames = new ArrayList<>();
        visitors.forEach(visitor -> {
            visitorFirstNames.add(visitor.getUserFirst());
            visitorLastNames.add(visitor.getUserLast());
            totalVisits[0] = totalVisits[0] + visitor.getVisitCount();
            cachedVisitorsList.put(visitor.getUserFirst() + visitor.getUserLast(), visitor);
            visitorDtos.add(VisitorDto.builder()
                    .visitCount(visitor.getVisitCount())
                    .userFirst(visitor.getUserFirst())
                    .userLast(visitor.getUserLast())
                    .build());
        });

        StatUtil.INSTANCE.setCachedVisitorsList(cachedVisitorsList);
        return VisitStatistics.builder()
                .totalVisits(totalVisits[0])
                .visitorFirstNames(visitorFirstNames)
                .visitorLastNames(visitorLastNames)
                .visitors(visitorDtos)
                .build();
    }
}

