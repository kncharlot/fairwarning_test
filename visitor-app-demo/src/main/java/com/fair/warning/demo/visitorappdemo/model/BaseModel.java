package com.fair.warning.demo.visitorappdemo.model;

import com.fair.warning.demo.visitorappdemo.Util.Util;
import com.fair.warning.demo.visitorappdemo.exception.ApiException;

public abstract class BaseModel {
    /**
     * Help print classes as json string
     *
     * @return
     */
    @Override
    public String toString() {
        try {
            return Util.classToString(this);
        } catch (ApiException e) {
            return super.toString();
        }
    }
}
