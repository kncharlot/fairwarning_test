package com.fair.warning.demo.visitorappdemo.Util;

import com.fair.warning.demo.visitorappdemo.model.Visitor;

import java.util.HashMap;
import java.util.Map;

public enum StatUtil {
    INSTANCE;
    private Map<String, Visitor> cachedVisitorsList = new HashMap<>();

    public Map<String, Visitor> getCachedVisitorsList() {
        return cachedVisitorsList;
    }

    public void setCachedVisitorsList(Map<String, Visitor> cachedVisitorsList) {
        this.cachedVisitorsList = cachedVisitorsList;
    }

    public void updateCache(Visitor visitor) {
        cachedVisitorsList.put(visitor.getUserFirst() + visitor.getUserLast(), visitor);
    }
}
