package com.fair.warning.demo.visitorappdemo.service.impl;

import com.fair.warning.demo.visitorappdemo.model.Visitor;
import com.fair.warning.demo.visitorappdemo.repository.VisitorRepository;
import com.fair.warning.demo.visitorappdemo.service.VisitorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VisitorServiceImpl implements VisitorService {
    @Autowired
    private VisitorRepository visitorRepository;

    public Visitor findVisitorByFirstAndLastName(String first, String last) {
        return visitorRepository.findDistinctByUserFirstAndAndUserLast(first, last);
    }

    @Override
    public List<Visitor> findVisitorsByFirstName(String first) {
        return visitorRepository.findDistinctByUserFirst(first);
    }

    @Override
    public List<Visitor> findVisitorsByLastName(String last) {
        return visitorRepository.findDistinctByUserLast(last);
    }

    public List<Visitor> findVisitorsByVisitCount(int visitCount) {
        return visitorRepository.findDistinctByVisitCount(visitCount);
    }

    public List<Visitor> findVisitorsByVisitCountGreaterThan(int visitCount) {
        return visitorRepository.findDistinctByVisitCountGreaterThan(visitCount);
    }

    public List<Visitor> findVisitorsByVisitCountLessThan(int visitCount) {
        return visitorRepository.findDistinctByVisitCountLessThan(visitCount);
    }

    public List<Visitor> findAll() {
        return (List<Visitor>) visitorRepository.findAll();
    }

    @Override
    public Visitor saveOrUpdate(Visitor visitor) {
        return visitorRepository.save(visitor);
    }
}
