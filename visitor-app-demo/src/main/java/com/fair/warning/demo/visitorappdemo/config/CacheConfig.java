package com.fair.warning.demo.visitorappdemo.config;

import com.fair.warning.demo.visitorappdemo.exception.ApiException;
import com.fair.warning.demo.visitorappdemo.service.VisitStatisticsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class CacheConfig implements CommandLineRunner {
    private static final Logger LOG = LoggerFactory.getLogger(CacheConfig.class);
    @Autowired
    private VisitStatisticsService visitStatisticsService;

    /**
     * Build cache of visitor's info.
     *
     * @param args
     * @throws Exception
     */
    @Override
    public void run(String... args) throws Exception {
        LOG.info("Building cache.");
        try {
            visitStatisticsService.buildVisitStatistics();
        } catch (Exception e) {
            throw new ApiException("Failed to build cache");
        }
    }
}