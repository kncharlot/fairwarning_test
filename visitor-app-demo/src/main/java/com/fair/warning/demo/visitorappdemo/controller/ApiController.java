package com.fair.warning.demo.visitorappdemo.controller;

import com.fair.warning.demo.visitorappdemo.Util.StatUtil;
import com.fair.warning.demo.visitorappdemo.Util.Util;
import com.fair.warning.demo.visitorappdemo.model.ErrorDetails;
import com.fair.warning.demo.visitorappdemo.model.VisitStatistics;
import com.fair.warning.demo.visitorappdemo.model.Visitor;
import com.fair.warning.demo.visitorappdemo.model.VisitorDto;
import com.fair.warning.demo.visitorappdemo.service.ErrorDetailsService;
import com.fair.warning.demo.visitorappdemo.service.VisitStatisticsService;
import com.fair.warning.demo.visitorappdemo.service.VisitorService;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class ApiController {
    private static final Logger LOG = LoggerFactory.getLogger(ApiController.class);
    @Autowired
    VisitStatisticsService visitStatisticsService;
    @Autowired
    VisitorService visitorService;
    @Autowired
    ErrorDetailsService errorDetailsService;

    @ApiOperation(value = "Get stats about user visits. Admin only.")
    @PostMapping("/visit-stats")
    public ResponseEntity getVisitStatistics(@Valid @RequestBody VisitorDto visitor) {
        LOG.info("getVisitStatistics");
        VisitStatistics visitStatistics;

        if (isAdmin(visitor))
            visitStatistics = visitStatisticsService.buildVisitStatistics();
        else
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);

        if (null != visitStatistics)
            return ResponseEntity.ok(visitStatistics);
        else
            return ResponseEntity.noContent().build();
    }

    @ApiOperation(value = "Get visitors by visit count. Visitor gets only the number of visitors. Admin gets info.")
    @PostMapping("/visitCount/{visitCount}")
    public ResponseEntity getVisitorByVisitCount(@Valid @PathVariable int visitCount,
                                                 @RequestBody(required = false) VisitorDto requestingVisitor) {
        LOG.info("getVisitorByVisitCount");

        List<Visitor> visitors = visitorService.findVisitorsByVisitCount(visitCount);

        if (Util.isValidList(visitors)) {
            List<VisitorDto> result = visitors.stream()
                    .map(visitor -> VisitorDto.builder()
                            .userFirst(visitor.getUserFirst())
                            .userLast(visitor.getUserLast())
                            .visitCount(visitor.getVisitCount())
                            .build()).collect(Collectors.toList());
            if (isAdmin(requestingVisitor))
                return ResponseEntity.ok(result);
            else
                return ResponseEntity.ok(result.size() + " returning visitor(s) so far.");
        } else
            return ResponseEntity.noContent().build();
    }

    @ApiOperation(value = "Get user visit stats. Admin gets info. " +
            "But visitor get only visit count if the user exists.")
    @PostMapping("/visitor")
    public ResponseEntity getUserVisitStatistics(@Valid @RequestBody VisitorDto requestingVisitor) {
        LOG.info("getUserVisitStatistics");
        Visitor returningVisitor = null;
        if (null != requestingVisitor) {
            //check cache
            returningVisitor = StatUtil.INSTANCE.getCachedVisitorsList().get(requestingVisitor.getUserFirst() + requestingVisitor.getUserLast());
            if (null == returningVisitor) {
                LOG.info("Visitor not cached. Looking in the table.");
                returningVisitor = visitorService.findVisitorByFirstAndLastName(requestingVisitor.getUserFirst(), requestingVisitor.getUserLast());
            }
        }
        if (null != returningVisitor) {
            if (isAdmin(requestingVisitor))
                return ResponseEntity.ok(Util.buildObjectFromOriginal(returningVisitor, VisitorDto.class));
            else
                return ResponseEntity.ok(returningVisitor.getVisitCount() + " visit(s) so far.");
        } else
            return ResponseEntity.notFound().build();
    }

    @ApiOperation(value = "Get error details by error Id.")
    @PostMapping("/errors")
    public ResponseEntity getErrorDetails(@Valid Long errorId, @Valid @RequestBody VisitorDto requestingVisitor) {
        LOG.info("getErrorDetails");
        ErrorDetails errorDetails = null;
        Visitor returningVisitor = null;
        if (null != requestingVisitor) {
            //check cache
            returningVisitor = StatUtil.INSTANCE.getCachedVisitorsList().get(requestingVisitor.getUserFirst() + requestingVisitor.getUserLast());
            if (null == returningVisitor) {
                LOG.info("Visitor not cached. Looking in the table.");
                returningVisitor = visitorService.findVisitorByFirstAndLastName(requestingVisitor.getUserFirst(), requestingVisitor.getUserLast());
            }
        }
        if (null != returningVisitor && isAdmin(requestingVisitor)) {
            errorDetails = errorDetailsService.findByProcessId(errorId);
            return ResponseEntity.ok(errorDetails);
        } else
            return ResponseEntity.notFound().build();
    }

    private boolean isAdmin(VisitorDto visitor) {
        if (null != visitor)
            return StringUtils.equalsIgnoreCase(visitor.getUserFirst(), "admin") &&
                    StringUtils.equalsIgnoreCase(visitor.getUserLast(), "admin");
        else return false;
    }
}
