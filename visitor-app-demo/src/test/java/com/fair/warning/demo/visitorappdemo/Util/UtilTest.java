package com.fair.warning.demo.visitorappdemo.Util;

import com.fair.warning.demo.visitorappdemo.exception.ApiException;
import com.fair.warning.demo.visitorappdemo.model.Visitor;
import com.fair.warning.demo.visitorappdemo.model.VisitorDto;
import org.junit.Assert;
import org.junit.Test;

public class UtilTest {

    @Test
    public void classToString() throws ApiException {
        String result = "{\n" +
                "\"visitcount\":0,\n" +
                "\"userfirst\":\"Mufasa\",\n" +
                "\"userlast\":\"King\"\n" +
                " }";
        Visitor visitor = Visitor.builder()
                .id(51)
                .userFirst("Mufasa")
                .userLast("King")
                .build();

        Assert.assertEquals("Wrong visitor print", Util.classToString(visitor), result);
        Assert.assertEquals("Wrong visitor print", Util.classToString(null), "");
    }

    @Test
    public void isValidString() {
        String example = "";
        String nullString = null;
        String nullBug = "null";
        String other = "other";

        Assert.assertFalse(Util.isValidString(example));
        Assert.assertFalse(Util.isValidString(nullBug));
        Assert.assertFalse(Util.isValidString(nullString));
        Assert.assertTrue(Util.isValidString(other));
    }

    @Test
    public void buildObjectFromOriginal() {
        Visitor visitor = Visitor.builder()
                .userLast("testLast")
                .userFirst("testFirst")
                .visitCount(51)
                .build();
        VisitorDto dto = VisitorDto.builder()
                .userLast("testLast")
                .userFirst("testFirst")
                .visitCount(51)
                .build();

        Assert.assertEquals("Wrong dto made", dto.toString(), Util.buildObjectFromOriginal(dto, Visitor.class).toString());

    }
}